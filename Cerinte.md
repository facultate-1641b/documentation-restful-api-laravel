# Cerințe RESTful API Laravel
Construirea unui RESTful API pentru un sistem de piață folosind Laravel.
## Studenți
1. Gîrz Sebastian - 1641A
2. Muresan Sebastian - 1641B
3. Roibu Remus - 1641B
4. Socaciu Călin - 1641B
## Prof. Coordonator
1. Mang Ioan
## Cerințe
1. Folosim Laravel, PHP artisan pentru generarea de cod și componente.
2. Rutele și controalarele Laravel Resource (ideale pentru API-urile RESTful).
3. Răspunsuri în format JSON complet acceptate pentru un API JSON RESTful.
4. Gestionarea tuturor tipurilor de excepții și erori de la handleruri Laravel.
5. Generarea structurii bazei de date folosind migrațiile Laravel.
6. Introducerea automată a datelor false în baza de date prin „falsificator”, folosind factories Laravel  și seeders.
7. Utilizarea Laravel Passport pentru a proteja API-ul RESTful cu OAuth.
8. Paginarea rezultatelor folosind colecțiile Laravel.

# Proiect

### Obținerea Laravel folosind Composer
- Instalarea frameworkului Laravel, cu ajutorul terminalului, folosind comanda:
> * `composer create-project laravel/laravel [filename]` 
- Pentru a porni serverul pe local, vom folosi comanda:
> * `php artisan serve`
### Alte comenzi folosite
- Pentru creare unui Request am folosit comanda:
> * `php artisan make:request NameRequest`
- Pentru creare unei Exception am folosit comanda:
> * `php artisan make:exception NameException`
- Pentru crearea unui Controller am folosit comanda:
> * `php artisan make:controller NameController --resource`
- Pentru creare unui Model am folosit comanda:
> * `php artisan make:model NameModel`
- Pentru creare unui fisier de Migrare am folosit comanda:
> * `php artisan make:migration NameTableMigration`
- Pentru crearea unui Resource am folosit comanda:
> * `php artisan make:resource NameResource`
- Pentru creare unui Factory am folosit comanda:
> * `php artisan make:factory NameFactory`

### Technologii folosite
Ca și framework am folosit laravel 8, limbaje folosite php, dependințe faker, tinker, ui și passport.

### Cum funționează aplicația?
Prima oară trebuie instalate dependințele de care avem nevoie, introducrea comenzilor în terminal sub urmatoarea formă:
> * `composer install`
> * `composer require laravel/ui:^2.4`
> * `php artisan ui vue --auth`
> * `composer require laravel/passport`

Pentru a putea folosi aplicația noastră sunteți nevoit să faceți conexiunea la baza de date în fișierul .env.
Exemplu:

> * `DB_CONNECTION=mysql`
> * `DB_HOST=127.0.0.1`
> * `DB_PORT=3306`
> * `DB_DATABASE=restfulapi`
> * `DB_USERNAME=root`
> * `DB_PASSWORD=`

După în interiorul terminalului introducem comanda `php artisan migrate`, `php artisan ui:auth` și `php artisan passport:install`, pentru ca Laravel sa construiască cu ajutorul fișierelor de migrare găsite la ruta: eapi/database/migrations. Sau se poate importa fișierul nostru .sql care se află la ruta eapi/db/restfulapi.sql.

În cazul în care nu doriți să importați baza noastră de date, vă oferim niște fișiere de populare a bazei de date, Seeders, prin introducere în terminal a comenzii `php artisan db:seed`.

Se poate testa pe aplicația Postman, se creeaza un Environment si se aduagă variabila auth și se aduagă în current value pe care îl puteți afla apelând ruta localhost:8000/oauth/token cu un body:

Exemplu:

> * `{`
> * `    "grant_type" : "password",`
> * `    "client_id" : "id oauth_client",`
> * `    "client_secret" : "secret oauth_client",`
> * `    "username" : "exemplu@gmail.com",`
> * `    "password" : "password"`
> * `}`

Output: 

> * `{`
> * `    "token_type": "Bearer",`
> * `    "expires_in": 31536000,`
> * `    "access_token": "accessToken",`
> * `    "refresh_token": "refreshToken"`
> * `}`

Se adaugă în current value Bearer accessToken.

Pentru a putea afișa toate produsele se apelează ruta http://localhost:8000/api/products, pentru a afișa un singur produse se apelează ruta localhost:8000/api/products/idproduct, pentru a putea crea un produs se apelează ruta localhost:8000/api/products cu method POST și cu headers:

> * `Content-Type` `application/json`
> * `Accept` `application/json`
> * `Authorization` `{{auth}}`

și cu body:

Exemplu:

> * `{`
> * `    "name" : "Iphone X",`
> * `    "description" : "The best phone!",`
> * `    "price" : "700",`
> * `    "stock" : "10",`
> * `    "discount" : "20"`
> * `}`

Pentru a updata un produs v-om apela ruta localhost:8000/api/products/idproduct cu method PUT, aceleiaşi headers ca și exemplu de mai sus și în body adaugăm fieldurile pe care dorim să le modificăm. Pentru delete se modifică method în DELETE și completat headers.

Pentru a afișa reviews v-om apela ruta localhost:8000/api/products/idproduct/reviews fară a fi nevoie să completăm headers. Pentru a modifica un review apelăm aceeași rută doar cu methoda PUT completând headers si body. Iar pentru a șterge folosim method DELETE ce aceeași rută.




